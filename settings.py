"""Displays the settings window with corresponding functions"""
import metadata_interface
from sqlite_helper import SqliteHelper
from PyQt5 import  QtWidgets, uic
from PyQt5.QtWidgets import QFileDialog
import os

APP = QtWidgets.QApplication([])
DLG = uic.loadUi("settings_ui.ui")
error_dialog = QtWidgets.QErrorMessage()

# TODO: We need to update our database scheme and Titel should be Title

def create_new_database():
    """Creates Database notes and fills it with the necessary relations

    For the questions we are implementing SM2 https://www.supermemo.com/en/archives1990-2015/english/ol/sm2
    n is therefor is the n-th repetition
    ef is E-Factor: easiness factor reflecting the easiness of memorizing and retaining a given item in memory
    """
    notes = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    notes.create_table(
        """CREATE TABLE notes(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                Source TEXT,
                Title TEXT,
                TimestampCreated TEXT
                )"""
    )
    notes.create_table(
        """CREATE TABLE tags(
                id INTEGER,
                tag TEXT,
                PRIMARY KEY(id,tag)
                FOREIGN KEY(id)
                    REFERENCES notes (id))"""
    )
    notes.create_table(
        """CREATE TABLE questions(
                id INTEGER,
                n INTEGER,
                ef REAL,
                timestamp TEXT,
                question TEXT,
                PRIMARY KEY(id,question)
                FOREIGN KEY(id)
                    REFERENCES notes (id))"""
    )
    notes.create_table(
        """CREATE TABLE relations(
                id1 INTEGER,
                id2 INTEGER,
                Desc TEXT,
                PRIMARY KEY(id1,id2)
                FOREIGN KEY (id2)
                    REFERENCES notes (id)
                FOREIGN KEY(id1)
                    REFERENCES notes (id))"""
    )


def create_new_wiki():
    """Selects path for wiki, creates directory and fills new database"""
    path_ = str(QFileDialog.getExistingDirectory(None, "Select Directory"))
    path_ = path_ + "/notes"
    if not os.path.exists(path_):
        os.mkdir(path_)
    metadata_interface.set_notes_dir(path_)
    create_new_database()


def open_existing_wiki():
    """Allows user to open an already existing wiki """
    path_ = str(QFileDialog.getExistingDirectory(None, "Select Wiki"))
    if os.path.basename(path_) != "notes":
        error_dialog.showMessage("That was not a notes folder :) You can create a new one though")
    else:
        metadata_interface.set_notes_dir(path_)


DLG.OpenWiki.clicked.connect(open_existing_wiki)
DLG.NewWiki.clicked.connect(create_new_wiki)

DLG.show()
APP.exec()
