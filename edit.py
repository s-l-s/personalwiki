"""Script to edit notes in database. Uses GUI from PyQt."""
from PyQt5 import QtWidgets, uic, QtGui, QtCore
import metadata_interface
import sys
from datetime import datetime
from sqlite_helper import SqliteHelper


APP = QtWidgets.QApplication([])
DLG = uic.loadUi("insert_ui.ui")

# TODO: Structure this part

# We tell QTextEdit where it has to look up for resources
directory = metadata_interface.get_path_to_notes() + str(sys.argv[1])
DLG.textEdit.document().setMetaInformation(
    QtGui.QTextDocument.DocumentUrl,
    QtCore.QUrl.fromLocalFile(directory).toString() + "/",
)
DLG.textEdit.set_path_to_note_dir(directory + "/")


def load_questions(note_id=sys.argv[1]):
    """load questions in text edit"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")

    questions = helper.id_to_questions(note_id)

    for question in questions:
        DLG.QuestionsEdit.appendPlainText(question[0])


def load_tags(note_id=sys.argv[1]):
    """load tags in text edit"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")

    questions = helper.id_to_tags(note_id)

    for question in questions:
        DLG.TagsEdit.appendPlainText(question[0])


def load_title(note_id=sys.argv[1]):
    """load title in text edit"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")

    title = helper.id_to_title(note_id)
    DLG.TitleEdit.setPlainText(title[0])


def load_source(note_id=sys.argv[1]):
    """load tags in text edit"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")

    source = helper.id_to_source(note_id)
    DLG.SourceEdit.setPlainText(source[0])


def load_timestamp_created(note_id=sys.argv[1]):
    """Returns timestamp when note was created"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    timestamp = helper.id_to_timestamp(note_id)
    DLG.TimestampCEdit.setPlainText(timestamp[0])


def load_html(note_id=sys.argv[1]):
    """load html file in editor"""
    path_to_note = get_path_to_note_from_id()
    html_string = get_html_as_string(path_to_note)
    DLG.textEdit.setHtml(html_string)


def get_path_to_note_from_id(note_id=sys.argv[1]):
    """returns path to note (not temp)"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    title = helper.id_to_title(note_id)
    path_to_note = metadata_interface.get_path_to_notes() + str(note_id) + "/" + str(title[0]) + ".html"
    return path_to_note


def get_html_as_string(path_to_html_file):
    """returns html file as a string"""
    with open(path_to_html_file, "r") as f:
        html = f.read()
    return html


def save_html_file(html_file):
    """saves html file which we already have"""
    path_to_note = get_path_to_note_from_id()
    with open(path_to_note,"w") as file:
        file.write(html_file)


# TODO: make as function or
DLG.QuestionsEdit.clear()
DLG.TagsEdit.clear()
DLG.TimestampCEdit.clear()
load_timestamp_created()
load_questions()
load_tags()
load_title()
load_source()
load_html()


def update_data():
    """main function of insert.py: Data from Text Fields are added to database"""

    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%dT%H:%M:%S")

    title = DLG.TitleEdit.toPlainText()
    timestamp_created = DLG.TimestampCEdit.toPlainText()
    source = DLG.SourceEdit.toPlainText()
    tags = DLG.TagsEdit.toPlainText()
    questions = DLG.QuestionsEdit.toPlainText()

    text = DLG.textEdit.toHtml()
    save_html_file(text)

    note_id = sys.argv[1]
    note_values_with_id = (source, title, timestamp_created, note_id)
    note_tags = list(tags.split("\n"))
    note_questions = list(questions.split("\n"))
    helper.update(note_id,note_values_with_id, note_tags, note_questions, timestamp)


DLG.AddButton.clicked.connect(update_data)

DLG.show()
APP.exec()
