"""Script to insert notes into database. Uses GUI from PyQt."""
import os
import json
from shutil import copyfile
from datetime import datetime
from PyQt5 import QtWidgets, uic
import metadata_interface

from sqlite_helper import SqliteHelper


APP = QtWidgets.QApplication([])
DLG = uic.loadUi("insert_ui.ui")


def create_temp_dir():
    """Creates the (temporary) directory to store note and metadata there later"""
    with open("metadata.json", "r") as json_file:
        json_data = json.load(json_file)
    make_copy = json_data["make_copy"]
    if make_copy:
        path_dir = json_data["path_dir"] + """temp//"""
        if not os.path.exists(path_dir):
            os.mkdir(path_dir)


create_temp_dir()  # if there is no temp dir, we create one


def insert_data():
    """main function of insert.py: Data from Text Fields are added to database"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    now: datetime = datetime.now()
    timestamp = now.strftime("%Y-%m-%dT%H:%M:%S")

    title = DLG.TitleEdit.toPlainText()
    source = DLG.SourceEdit.toPlainText()
    DLG.textEdit.save_as_html(title)
    tags = DLG.TagsEdit.toPlainText()
    questions = DLG.QuestionsEdit.toPlainText()

    note_values = (source, title, timestamp)
    note_tags = list(tags.split("\n"))
    note_questions = list(questions.split("\n"))
    helper.insert(note_values, note_tags, note_questions, timestamp)
    last_row_id_notes = helper.select(
        """select seq from sqlite_sequence where name="notes" """
    )[0][0]
    update_dir(last_row_id_notes)


DLG.AddButton.clicked.connect(insert_data)


def update_dir(new_dir_name):
    """When insertion was successful, we make temp dir the note dir"""
    with open("metadata.json", "r") as json_file:
        json_data = json.load(json_file)
    new_path_dir = json_data["path_dir"] + str(new_dir_name)
    old_path_dir = json_data["path_dir"] + """temp//"""
    os.rename(old_path_dir, new_path_dir)
    create_temp_dir()  # new temp dir is created


def copy_note(note_path):
    """depending on user preferences in metadata.json, we make a copy of a note, when added"""
    with open("metadata.json", "r") as json_file:
        json_data = json.load(json_file)
    copy_path = note_path
    make_copy = json_data["make_copy"]
    if make_copy:
        path_dir = json_data["path_dir"] + """temp//"""
        filename = os.path.basename(note_path)
        copy_path = path_dir + filename
        copyfile(note_path, copy_path)
    return copy_path


def create_note_data():
    """User can select File and Metadata is extracted"""
    note_path = QtWidgets.QFileDialog.getOpenFileName()[0]
    title = os.path.splitext(os.path.basename(note_path))[0]
    now: datetime = datetime.now()
    timestamp = now.strftime("%m/%d/%Y, %H:%M:%S")
    note_path = copy_note(note_path)
    DLG.TitleEdit.setPlainText(title)
    DLG.PathEdit.setPlainText(note_path)
    DLG.TimestampAEdit.setPlainText(timestamp)
    DLG.TimestampCEdit.setPlainText(timestamp)


DLG.LoadButton.clicked.connect(create_note_data)

DLG.show()
APP.exec()
