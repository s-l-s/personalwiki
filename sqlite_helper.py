"""A modul to easily execute sql queries on your database"""
import sqlite3


class SqliteHelper:
    """A class to easily execute sql queries on your database"""

    def __init__(self, name=None):
        self.conn = None
        self.cursor = None
        if name:
            self.open(name)

    def open(self, name):
        """Opens your database in the helper class"""

        try:
            self.conn = sqlite3.connect(name)
            self.cursor = self.conn.cursor()
        except sqlite3.Error:
            print("Failed connecting to database...")

    #
    def create_procedure(self):
        self.conn.create_function("CALCLEARNINTERVAL", 2, self.calc_learning_interval)

    def create_table(self, query):
        """creates a new table in your database. Needs whole query as Input

        Example: CREATE TABLE tags(
            id INTEGER,
            tag TEXT,
            PRIMARY KEY(id,tag)
            FOREIGN KEY(id)
                REFERENCES notes (id))
        """

        cursor = self.cursor
        cursor.execute(query)

    def edit(self, query):
        """Input query will be executed on database (in helper instance)"""
        cursor = self.cursor
        cursor.execute(query)
        self.conn.commit()

    def insert(self, note_values, note_tags, note_questions, timestamp):
        """inserts values into specific database notes.db

        for tags and questions it iterates over a the input list and inserts a row for
        every tag/question
        """

        cursor = self.cursor
        cursor.execute(
            (
                """INSERT INTO notes (Source,Title,TimestampCreated)
                VALUES (?,?,?)"""
            ),
            note_values,
        )
        note_id = cursor.lastrowid
        for i in note_tags:
            cursor.execute(
                """INSERT INTO tags VALUES ("""
                + str(note_id)
                + """,?) """,
                tuple(i.split(",")),
            )

        # TODO: E-Factor and n should be set in metadata.json
        for i in note_questions:
            tuple_ = (timestamp,i.split(",")[0])
            cursor.execute(
                """INSERT INTO questions VALUES ("""
                + str(note_id)
                + """,1, 2.5, ?, ?)""",
                tuple_,
            )

        self.conn.commit()

    def update(self, note_id, note_values_with_id, note_tags, note_questions,timestamp):
        """updates values into specific database notes.db"""

        cursor = self.cursor
        sql_update = """UPDATE notes 
                        SET Source = ?,
                            Title = ?,
                            TimestampCreated = ?
                        WHERE id = ?               
                     """
        cursor.execute(sql_update, note_values_with_id)
        # TODO: n and ef from metadata. Also try and except not good.
        for i in note_questions:
            try:
                tuple_ = (timestamp,i.split(",")[0])
                cursor.execute(
                    """INSERT INTO questions VALUES ("""
                    + str(note_id)
                    + """,1,2.5,?,?)""",
                    tuple_,
                )
            except sqlite3.Error:
                continue

        for i in note_tags:
            try:
                cursor.execute(
                    """INSERT INTO tags VALUES (""" + str(note_id) + """,?) """,
                    tuple(i.split(",")),
                )
            except sqlite3.Error:
                continue

        self.conn.commit()

    def select(self, query):
        """Input Query to return rows"""
        cursor = self.cursor
        cursor.execute(query)
        rows = cursor.fetchall()
        return rows

    def select_notes_by_name(self, note_name):
        """Input name of a note to recive rows with that name"""
        cursor = self.cursor
        cursor.execute("""SELECT * FROM 'notes' WHERE Title LIKE ?""", (note_name,))
        rows = cursor.fetchall()
        return rows

    def title_to_id(self, note_name):
        """Returns id from title of a note. Can be more than one id"""
        cursor = self.cursor
        cursor.execute("""SELECT id FROM 'notes' WHERE Title LIKE ?""", (note_name,))
        rows = cursor.fetchall()
        return rows

    def id_to_questions(self, id):
        """Returns questions from id of a note"""
        cursor = self.cursor
        cursor.execute("""SELECT question FROM questions WHERE id = ?""", (id,))
        rows = cursor.fetchall()
        return rows

    def id_to_tags(self, id):
        """Returns tags from id of a note"""
        cursor = self.cursor
        cursor.execute("""SELECT tag FROM tags WHERE id = ?""", (id,))
        rows = cursor.fetchall()
        return rows

    def id_to_title(self, id):
        """Returns title from id of a note"""
        cursor = self.cursor
        cursor.execute("""Select title FROM notes WHERE id = ?""", (id,))
        title = cursor.fetchone()
        return title

    def id_to_source(self, id):
        """Returns source from id of a note"""
        cursor = self.cursor
        cursor.execute("""Select source FROM notes WHERE id = ?""", (id,))
        source = cursor.fetchone()
        return source

    def id_to_timestamp(self, id):
        """Returns timestamp from id of a note"""
        cursor = self.cursor
        cursor.execute("""Select TimestampCreated FROM notes WHERE id = ?""", (id,))
        source = cursor.fetchone()
        return source

    def update_question(self,tuple_):
        cursor = self.cursor
        cursor.execute("""UPDATE questions SET ef = ?,n = ? WHERE id = ? AND question = ?""",tuple_)
        self.conn.commit()

    def calc_learning_interval(self,n,ef):
        if n < 1:
            print("Whoups. You cant have 0 repetitions")
        elif n == 1:
            return 1
        elif n == 2:
            return 6
        elif n > 2:
            n = self.calc_learning_interval(n - 1,ef) * ef
            return n
