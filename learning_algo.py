""" GUI and SM2-Algorithm for learning concepts"""
from PyQt5 import QtWidgets, uic, QtGui, QtCore
import metadata_interface
from random import choice
from os import system
from sqlite_helper import SqliteHelper

APP = QtWidgets.QApplication([])
DLG = uic.loadUi("learning.ui")
Question_instance = 0


class NextQuestion():
    """Instances are questions that user should learn."""

    def __init__(self,question):
        self.id = question[0]
        self.question_text = question[1]
        self.n = question[2]
        self.ef = question[3]
        self.difficulty = 4
        self.helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")

    def set_difficulty_question(self):
        """Difficulty is set depending on User Input via Qt GUI"""
        self.difficulty = DLG.horizontalSlider.value()

    def calculate_new_ef(self):
        if self.ef > 1.3:
            diff = self.difficulty
            self.ef = self.ef - 0.8 + 0.28 * diff - 0.02 * diff * diff
        else:
            self.ef = self.ef

    def calculate_new_n(self):
        diff = self.difficulty
        if diff < 3:
            self.n = 1
        else:
            self.n = self.n + 1

    def load_question_in_gui(self):
        """Displays Question in GUI"""
        DLG.questionEdit.setPlainText(self.question_text)

    def load_answer_in_gui(self):
        directory = metadata_interface.get_path_to_notes() + str(self.id)
        DLG.textEdit.document().setMetaInformation(
            QtGui.QTextDocument.DocumentUrl,
            QtCore.QUrl.fromLocalFile(directory).toString() + "/",
        )
        path_to_note = get_path_to_note_from_id(self.id)
        html_string = get_html_as_string(path_to_note)
        DLG.textEdit.setHtml(html_string)

    def get_id_of_question(self):
        return self.id

    def update_question_in_database(self):
        tuple_ = (self.ef,self.n,self.id,self.question_text)
        self.helper.update_question(tuple_)




def select_one_question():
    """ Select random question of the first 5 overdue questions"""
    questions = get_overdue_questions()[:5]
    print(questions)
    question = choice(questions)
    return question


def get_overdue_questions():
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    helper.create_procedure()
    questions = helper.select(
        """SELECT id,question,n,ef FROM questions WHERE julianday(datetime('now')) - julianday(datetime(timestamp)) >= CALCLEARNINTERVAL(n,ef)""")
    if not questions:
        QtWidgets.QMessageBox.about(DLG, "That's it", "Nice! All Questions have been answered")
        exit()
    else:
        print(str(len(questions)) + " questions to go! :)")
        return questions


# TODO: Strucure together with edit.py


def get_path_to_note_from_id(note_id):
    """returns path to note (not temp)"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    title = helper.id_to_title(note_id)
    path_to_note = metadata_interface.get_path_to_notes() + str(note_id) + "/" + str(title[0]) + ".html"
    return path_to_note


def get_html_as_string(path_to_html_file):
    """returns html file as a string"""
    with open(path_to_html_file, "r") as f:
        html = f.read()
    return html


def edit_note():
    note_id =  Question_instance.get_id_of_question()
    system("edit.py " + str(note_id))
    restart()


def clear_ui():
    # needs more elegant solution
    DLG.horizontalSlider.setValue(4)
    DLG.textEdit.clear()
    DLG.questionEdit.clear()


def restart():
    global Question_instance
    Question_instance.calculate_new_ef()
    Question_instance.calculate_new_n()
    Question_instance.update_question_in_database()
    clear_ui()
    Question_instance = NextQuestion(select_one_question())
    Question_instance.load_question_in_gui()
    print(Question_instance.get_id_of_question())

def show_answer():
    # for some reason it did not work to call method directly
    Question_instance.load_answer_in_gui()


Question_instance = NextQuestion(select_one_question())

Question_instance.load_question_in_gui()
print(Question_instance.get_id_of_question())

DLG.nextQuestion.clicked.connect(restart)
DLG.showAnswer.clicked.connect(show_answer)
DLG.editButton.clicked.connect(edit_note)

# TODO:Set push buttons shortcuts

DLG.show()
APP.exec()
